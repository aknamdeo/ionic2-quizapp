import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { TabsPage } from '../pages/tabs/tabs';
import { SignInPage } from '../pages/sign-in/sign-in';
import * as firebase from 'firebase';


const config = {
    apiKey: "AIzaSyA4AErW9TH61yvVW4gW8r1qc4YTPaj30ww",
    authDomain: "self-assessment-2eec3.firebaseapp.com",
    databaseURL: "https://self-assessment-2eec3.firebaseio.com",
    projectId: "self-assessment-2eec3",
    storageBucket: "",
    messagingSenderId: "790619698259"
  };

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage:any = SignInPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    firebase.initializeApp(config);
  }
}
