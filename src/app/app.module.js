var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ProgressPage } from '../pages/progress/progress';
import { ContactPage } from '../pages/contact/contact';
import { QuizIndexPage } from '../pages/quiz-index/quiz-index';
import { QuizDetailPage } from '../pages/quiz-detail/quiz-detail';
import { QuizMainPage } from '../pages/quiz-main/quiz-main';
import { QuizReviewPage } from '../pages/quiz-review/quiz-review';
import { QuizResultPage } from '../pages/quiz-result/quiz-result';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QuestionBankProvider } from '../providers/question-bank/question-bank';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                ProgressPage,
                ContactPage,
                QuizIndexPage,
                QuizDetailPage,
                QuizMainPage,
                QuizReviewPage,
                QuizResultPage,
                TabsPage
            ],
            imports: [
                BrowserModule,
                IonicModule.forRoot(MyApp, { tabsHideOnSubPages: true }),
                IonicStorageModule.forRoot(),
                HttpModule
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                ProgressPage,
                ContactPage,
                QuizIndexPage,
                QuizDetailPage,
                QuizMainPage,
                QuizReviewPage,
                QuizResultPage,
                TabsPage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                QuestionBankProvider
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map