import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ProgressPage } from '../pages/progress/progress';
import { ContactPage } from '../pages/contact/contact';
import { QuizIndexPage } from '../pages/quiz-index/quiz-index';
import { QuizDetailPage } from '../pages/quiz-detail/quiz-detail';
import { QuizMainPage } from '../pages/quiz-main/quiz-main';
import { QuizReviewPage } from '../pages/quiz-review/quiz-review';
import { QuizResultPage } from '../pages/quiz-result/quiz-result';
import { SignInPage } from '../pages/sign-in/sign-in';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QuestionBankProvider } from '../providers/question-bank/question-bank';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { FirebaseProvider } from '../providers/firebase/firebase';


@NgModule({
  declarations: [
    MyApp,
    ProgressPage,
    ContactPage,
    QuizIndexPage,
    QuizDetailPage,
    QuizMainPage,
    QuizReviewPage,
    QuizResultPage,
    SignInPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{ tabsHideOnSubPages: true }),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProgressPage,
    ContactPage,
    QuizIndexPage,
    QuizDetailPage,
    QuizMainPage,
    QuizReviewPage,
    QuizResultPage,
    SignInPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QuestionBankProvider,
    FirebaseProvider
  ]
})
export class AppModule {}
