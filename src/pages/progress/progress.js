var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Chart } from 'chart.js';
var ProgressPage = /** @class */ (function () {
    function ProgressPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ProgressPage.prototype.ionViewDidLoad = function () {
        this.lineChart = new Chart(this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                labels: ["P1", "P2", "P3", "P4", "P5", "P6", "P7"],
                datasets: [
                    {
                        label: "Score",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [65, 59, 80, 81, 56, 55, 40],
                        spanGaps: false,
                    }
                ]
            }
        });
    };
    __decorate([
        ViewChild('barCanvas'),
        __metadata("design:type", Object)
    ], ProgressPage.prototype, "barCanvas", void 0);
    __decorate([
        ViewChild('doughnutCanvas'),
        __metadata("design:type", Object)
    ], ProgressPage.prototype, "doughnutCanvas", void 0);
    __decorate([
        ViewChild('lineCanvas'),
        __metadata("design:type", Object)
    ], ProgressPage.prototype, "lineCanvas", void 0);
    ProgressPage = __decorate([
        Component({
            selector: 'page-progress',
            templateUrl: 'progress.html'
        }),
        __metadata("design:paramtypes", [NavController])
    ], ProgressPage);
    return ProgressPage;
}());
export { ProgressPage };
//# sourceMappingURL=progress.js.map