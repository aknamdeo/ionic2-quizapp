import { Component , ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { QuizIndexPage } from '../quiz-index/quiz-index';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';


@Component({
  selector: 'page-progress',
  templateUrl: 'progress.html'
})
export class ProgressPage {
    @ViewChild('barCanvas') barCanvas;
    @ViewChild('doughnutCanvas') doughnutCanvas;
    @ViewChild('lineCanvas') lineCanvas;

    barChart: any;
    doughnutChart: any;
    lineChart: any;
    public plabel=[];
    public pdata=[];
    public parr=[];


    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams, 
        private storage:Storage,
        private toastCtrl: ToastController) 
        {
        this.parr = navParams.get("progress");
            console.log('in');
            this.storage.get('progress').then((pscore) => {
              if(pscore!=null){
                this.setGraphData(pscore);
              }
            });
    }

    setGraphData(arr){
        for(let i=0 ; i<arr.length;i++){
            this.plabel.push("P"+i);
            this.pdata.push(arr[i]);
        }
        this.loadChart(this.plabel,this.pdata);
        //console.log(this.plabel, this.pdata);
    }


    loadChart(plabel,pdata){
      this.lineChart = new Chart(this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                labels: plabel,
                datasets: [
                    {
                        label: "Score",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: pdata,
                        spanGaps: false,
                    }
                ]
            }
        });
    }

    ionViewDidLoad() {
      
    }  


   goToWeb(){
      this.navCtrl.push(TabsPage,{index: "2"});
   }

   clearProgress(){
       this.storage.clear();
       this.lineChart.destroy();;
       console.log("Clear storage");
       this.presentToast("Storage Cleared");
   }

   backToQuiz(){
       this.navCtrl.push(TabsPage,{index: "0"});
   }



   presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 3000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
}
