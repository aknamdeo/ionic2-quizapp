var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionBankProvider } from '../../providers/question-bank/question-bank';
import { QuizIndexPage } from '../quiz-index/quiz-index';
import { QuizMainPage } from '../quiz-main/quiz-main';
/**
 * Generated class for the QuizDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuizDetailPage = /** @class */ (function () {
    function QuizDetailPage(navCtrl, navParams, questionBank) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.questionBank = questionBank;
        this.quizIndex = null;
        this.quizInstruction = "";
        this.quizIndex = navParams.get("quizIndex");
        this.quizInstruction = questionBank.myQuiz[this.quizIndex].instruction;
    }
    QuizDetailPage.prototype.ionViewDidLoad = function () {
        console.log(this.quizIndex);
    };
    QuizDetailPage.prototype.goToQuizIndex = function () {
        this.navCtrl.push(QuizIndexPage);
    };
    QuizDetailPage.prototype.goToQuizMain = function () {
        this.navCtrl.push(QuizMainPage, {
            quizIndex: this.quizIndex
        });
    };
    QuizDetailPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-quiz-detail',
            templateUrl: 'quiz-detail.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, QuestionBankProvider])
    ], QuizDetailPage);
    return QuizDetailPage;
}());
export { QuizDetailPage };
//# sourceMappingURL=quiz-detail.js.map