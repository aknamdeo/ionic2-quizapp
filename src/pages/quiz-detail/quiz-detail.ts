import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionBankProvider } from '../../providers/question-bank/question-bank';
import { QuizIndexPage } from '../quiz-index/quiz-index';
import { QuizMainPage } from '../quiz-main/quiz-main';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the QuizDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-detail',
  templateUrl: 'quiz-detail.html',
})
export class QuizDetailPage {
	public quizIndex=null;
	public quizInstruction="";
	public quiz = "";
	public qbank;

	constructor(public navCtrl: NavController, public navParams: NavParams , public questionBank: QuestionBankProvider, private storage:Storage) {
		this.quizIndex = navParams.get("quizIndex");
		this.storage.get("qbank").then((val) => {
			console.log(val);
			this.quizInstruction = val[this.quizIndex].instruction;
			this.quiz = val[this.quizIndex];
			this.qbank = val;
			this.setAttemptStoreage();
		});
		
	}

	ionViewDidLoad() {
		console.log(this.quizIndex);
	}

	public goToQuizIndex(){
		this.navCtrl.push(TabsPage,{index: "0"});
	}

	public goToQuizMain(){
		this.navCtrl.push(QuizMainPage,
		{
			quizIndex: this.quizIndex,
			questionIndex : 0,
			qbank: this.qbank

		});
	}


	setAttemptStoreage = function () {
      this.quiz.questions.forEach((value, key, index) => {
        let question = { 
            "qid" : value.qid, 
            "type" : value.type, 
            "correct" : value.correct, 
            "status" : "Un-Attempted", 
            "response" : "", 
            "score" : 0 
          };
          this.storage.set("q_"+ value.qid , question ).then((val)=>{
        });
      });
    };

}
