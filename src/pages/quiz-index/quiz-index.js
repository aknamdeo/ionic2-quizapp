var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionBankProvider } from '../../providers/question-bank/question-bank';
import { QuizDetailPage } from '../quiz-detail/quiz-detail';
/**
 * Generated class for the QuizIndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuizIndexPage = /** @class */ (function () {
    function QuizIndexPage(navCtrl, navParams, questionBank) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.questionBank = questionBank;
    }
    QuizIndexPage.prototype.load = function () {
        this.quizes = this.questionBank.load();
        console.log(this.quizes);
    };
    QuizIndexPage.prototype.ionViewDidLoad = function () {
        this.load();
    };
    QuizIndexPage.prototype.goToQuiz = function (i) {
        console.log("quiz page");
        this.navCtrl.push(QuizDetailPage, {
            quizIndex: i
        });
    };
    QuizIndexPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-quiz-index',
            templateUrl: 'quiz-index.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            QuestionBankProvider])
    ], QuizIndexPage);
    return QuizIndexPage;
}());
export { QuizIndexPage };
//# sourceMappingURL=quiz-index.js.map