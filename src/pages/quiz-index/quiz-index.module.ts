import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizIndexPage } from './quiz-index';

@NgModule({
  declarations: [
    QuizIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizIndexPage),
  ],
})
export class QuizIndexPageModule {}
