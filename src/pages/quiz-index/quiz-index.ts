import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionBankProvider } from '../../providers/question-bank/question-bank';
import { QuizDetailPage } from '../quiz-detail/quiz-detail';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the QuizIndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-index',
  templateUrl: 'quiz-index.html',
})
export class QuizIndexPage {
	public quizes : any
		constructor( 
				public navCtrl: NavController, 
				public navParams: NavParams , 
				public questionBank: QuestionBankProvider,
				private storage:Storage
			){
	}
	

	public load(){
		this.quizes=[];
		var that = this;

		this.questionBank.getData(function(data){
			console.log(that.quizes,"in");
			that.quizes = data;
  			that.storage.set('qbank',data);
		})
	} 


	public ionViewDidLoad() {
        this.load();
    }


    public goToQuiz(i){
    	this.navCtrl.push(QuizDetailPage,
    	{
    		quizIndex: i
    	});
    }
}
