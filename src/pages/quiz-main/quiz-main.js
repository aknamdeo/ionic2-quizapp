var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionBankProvider } from '../../providers/question-bank/question-bank';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the QuizMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuizMainPage = /** @class */ (function () {
    function QuizMainPage(navCtrl, navParams, questionBank, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.questionBank = questionBank;
        this.storage = storage;
        this.quizIndex = 0;
        this.qid = 1;
        this.currentPosition = 0;
        this.timeOut = false;
        this.currentQuestion = null;
        this.quiz = null;
        this.attempt = null;
        this.currentResponse = null;
        this.updatePage = function (mov) {
            var _this = this;
            this.storage.set("q_" + (this.currentPosition - 1), this.currentResponse);
            this.storage.get("q_" + (this.currentPosition)).then(function (val) {
                _this.currentResponse = val;
                _this.currentQuestion = _this.quiz.questions[_this.currentPosition];
            });
        };
        this.quizIndex = navParams.get("quizIndex");
        this.quiz = questionBank.myQuiz[this.quizIndex];
        this.currentQuestion = 1;
    }
    QuizMainPage.prototype.ionViewDidLoad = function (page) {
        console.log('ionViewDidLoad QuizMainPage');
        this.updatePage(0);
    };
    QuizMainPage.prototype.gotoNext = function () {
        var valid = this.hasNext();
        if (valid == true) {
            this.updatePage(1);
        }
        else {
            this.updatePage(0); // reload same to set attempt var
            this.attempt = this.attempt;
            var reviewPth = ('#/review/' + this.qid);
            window.location.href = (reviewPth);
        }
    };
    ;
    QuizMainPage.prototype.gotoPrev = function () {
        var valid = this.hasPrev();
        if (valid === true) {
            this.updatePage(-1);
        }
        else {
            // Reached first question no action needed // can disable button
        }
    };
    ;
    QuizMainPage.prototype.gotoReview = function () {
        this.storage.clear();
    };
    QuizMainPage.prototype.hasNext = function () {
        return !(this.currentPosition >= this.quiz.questions.length - 1);
    };
    ;
    QuizMainPage.prototype.hasPrev = function () {
        return !(this.currentPosition == 0);
    };
    ;
    QuizMainPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-quiz-main',
            templateUrl: 'quiz-main.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, QuestionBankProvider, Storage])
    ], QuizMainPage);
    return QuizMainPage;
}());
export { QuizMainPage };
//# sourceMappingURL=quiz-main.js.map