import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizMainPage } from './quiz-main';

@NgModule({
  declarations: [
    QuizMainPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizMainPage),
  ],
})
export class QuizMainPageModule {}
