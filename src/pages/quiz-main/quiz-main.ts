import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionBankProvider } from '../../providers/question-bank/question-bank';
import { Storage } from '@ionic/storage';
import { QuizReviewPage } from '../quiz-review/quiz-review';

/**
 * Generated class for the QuizMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-main',
  templateUrl: 'quiz-main.html',
})
export class QuizMainPage {
  public quizIndex = 0;
  public questionIndex = 0;
  public qid = 1;
  public currentPosition = 0;
  public timeOut = false;
  public currentQuestion = ""; 
  public quiz= [];
  public attempt = "";
  public currentResponse = "";
  public myQuiz;



	constructor(public navCtrl: NavController, public navParams: NavParams, public questionBank: QuestionBankProvider, private storage:Storage) {
    this.quizIndex = navParams.get("quizIndex");
    this.questionIndex = navParams.get("questionIndex");
    this.myQuiz = navParams.get("qbank"); 
      this.storage.set('currentQuiz',this.quizIndex).then((val)=>{
       console.log("done");
      });

    this.quiz.push(this.myQuiz[this.quizIndex]);
    if(this.questionIndex >= 0 ){
      this.currentPosition = this.questionIndex + 1;
    }else{
		  this.currentPosition = 1;
    }
	}

  

	ionViewDidLoad(page) {
    if(this.questionIndex >= 0 ){ 
      this.currentPosition = this.questionIndex + 1;
      this.currentQuestion = this.quiz[0].questions[this.questionIndex];
      console.log('this',this.questionIndex);
      this.updatePage(0,'same');
    }else{
      this.updatePage(0,'same');
    }
	}


  public onSelectionChange(resp){
    this.currentResponse=resp;
  }

	gotoNext() {
        var valid = this.hasNext();
        if (valid == true) {
            this.updatePage(1,'same');
        } else {
            this.gotoReview(); // reload same to set attempt var
        }
    };
    
    
    
    gotoPrev(){
        var valid = this.hasPrev();
        if (valid === true) {
            this.updatePage(-1,'same');
        } else {
            // Reached first question no action needed // can disable button
        }
    };



    gotoReview(){
      //this.storage.clear();
      this.updatePage(0,'review');
    }



    updatePage = function (mov, page) {
      this.storage.get("q_"+ (this.currentPosition)).then((val) => {
        if(this.currentResponse!=""){
          val.response=this.currentResponse;
          val.status="Attempted";
        }
        if(val.response==val.correct){
          val.score=1;
        }
        this.storage.set("q_"+ (this.currentPosition), val).then((val)=>{
            if(page=='review'){
                this.navCtrl.push(QuizReviewPage,{
                att: this.storage,
                qbank:this.myQuiz
              });
            }
            if(mov!=0){
              this.currentPosition += mov; 
              this.storage.get("q_"+ (this.currentPosition)).then((val) => {
                this.currentResponse = val.response;
                this.currentQuestion = this.quiz[0].questions[this.currentPosition - 1];
              });  
            }else{
              this.currentResponse=val.response;
              console.log('mov',val.response);
            }
        });
      });
    };



    setAttemptStoreage = function () {
      this.quiz[0].questions.forEach((value, key, index) => {
        let question = { 
            "qid" : value.qid, 
            "type" : value.type, 
            "correct" : value.correct, 
            "status" : "Un-Attempted", 
            "response" : "", 
            "score" : 0 
          };
          this.storage.set("q_"+ value.qid , question ).then((val)=>{
        });
      });
    };



    hasNext(){
      return !(this.currentPosition >= this.quiz[0].questions.length - 1);
    };



    hasPrev(){
      return !(this.currentPosition == 1);
    };



}
