import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { QuizIndexPage } from '../quiz-index/quiz-index';
import { ProgressPage } from '../progress/progress';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the QuizResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-result',
  templateUrl: 'quiz-result.html',
})
export class QuizResultPage {
	public myAtt=[];
  	public atObj;
  	public score=0;
  	public perScore=0;
	
	constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage) {
	 	this.atObj= navParams.get("att");
	 	this.score = navParams.get("totalScore");

    	this.atObj.forEach((value, key, index) => {
          if(key.substr(0,2)=='q_'){
            this.myAtt.push(value);
            this.myAtt.sort(function(a,b){ return a.qid-b.qid });
          }
  		})
	}

	totalScore(){
		let score=0;
		for(let i=0;i<this.myAtt.length;i++){
			  if(this.myAtt[i].score>0){
	            score++;
	          }
  		}
  		return score;
	}


	iamCorrect(qid){
		for(let i=0;i<this.myAtt.length;i++){
		  if(this.myAtt[i].qid==qid){
            this.myAtt[i].score=1;
          }
  		}
  		this.score=this.totalScore();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad QuizResultPage');

	}

	backToQuiz(){
		//get all progress
		//clear all storage
		//push current score and set progress
		this.storage.get('progress').then((pscore) => {
			if(pscore!=null){
				pscore.push((this.score/this.myAtt.length)*100);
			}else{
				pscore=[0];
				pscore.push((this.score/this.myAtt.length)*100);
			}
			this.storage.clear();
			this.storage.set("progress", pscore).then((progress)=>{
				this.navCtrl.push(TabsPage, {index:1});

			})
		});

		
	}

}
