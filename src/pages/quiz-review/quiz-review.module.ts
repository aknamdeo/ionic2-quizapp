import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizReviewPage } from './quiz-review';

@NgModule({
  declarations: [
    QuizReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizReviewPage),
  ],
})
export class QuizReviewPageModule {}
