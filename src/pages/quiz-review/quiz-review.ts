import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { QuizMainPage } from '../quiz-main/quiz-main';
import { QuizResultPage } from '../quiz-result/quiz-result';

/**
 * Generated class for the QuizReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-review',
  templateUrl: 'quiz-review.html',
})


export class QuizReviewPage {
	public myAtt=[];
  public review=[];
  public atObj;
  public currQuersion;
  public myQuiz;

  	constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage) {
 		this.atObj= navParams.get("att");
    this.currQuersion = navParams.get("currQuestion");
    this.myQuiz = navParams.get("qbank"); 

    	this.atObj.forEach((value, key, index) => {
          if(key.substr(0,2)=='q_'){
            this.myAtt.push(value);
            this.myAtt.sort(function(a,b){ return a.qid-b.qid});
          }
  		})
  	}

  goToQuestion(q){
    this.atObj.get('currentQuiz').then((val) => {
      this.navCtrl.push(QuizMainPage,
      {
        questionIndex: q,
        quizIndex: val,
        qbank: this.myQuiz
      });   
    });
  }  


  backToQuiz(){
    this.goToQuestion(this.currQuersion);
  }


  totalScore(){
    let score=0;
    for(let i=0;i<this.myAtt.length;i++){
        if(this.myAtt[i].score>0){
              score++;
            }
      }
      return score;
  }

  finishQuiz(){
    let totScore = this.totalScore();
    this.navCtrl.push(QuizResultPage,
      {
        att: this.atObj,
        totalScore: totScore
      });    
  }


  ionViewDidLoad() {
    this.review=this.myAtt;
  }

}
