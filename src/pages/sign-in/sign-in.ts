import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
//import { LoggerService } from '../../services/log4ts/logger.service';
import { TabsPage } from '../tabs/tabs';


/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  credentialsForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder
              ) {

    this.credentialsForm = this.formBuilder.group({
      email: [''],
      name: [''],
      password: ['']
    });
  }

  onSignIn() {
  	console.log("Testing");
  	this.navCtrl.push(TabsPage,{index: "0"});
    //this.logger.info('SignInPage: onSignIn()');
  }

  onForgotPassword() {
  	console.log("Forget");
    //this.logger.info('SignInPage: onForgotPassword()');
  }
}
