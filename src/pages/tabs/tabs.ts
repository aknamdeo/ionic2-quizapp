import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProgressPage } from '../progress/progress';
import { ContactPage } from '../contact/contact';
import { QuizIndexPage } from '../quiz-index/quiz-index';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = QuizIndexPage;
  tab2Root = ProgressPage;
  tab3Root = ContactPage;
  public index;
  constructor(public navCtrl: NavController, public navParams: NavParams, ) {
  	this.index = navParams.get('index')
  	console.log(this.index)
  }
}
