var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
/*
  Generated class for the QuestionBankProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var QuestionBankProvider = /** @class */ (function () {
    function QuestionBankProvider(http) {
        this.http = http;
        this.myQuiz = [];
        console.log('Hello QuestionBankProvider Provider');
    }
    QuestionBankProvider.prototype.load = function () {
        this.myQuiz = [
            {
                "id": 0,
                "title": "v360 questionnaire - 1",
                "instruction": "<ul><li>Quiz contain 50 text type Question </li><li>There is no time limit</li><li>Score will be shown after end of quiz</li><li>After review, attempt will be destroyed</li>",
                "duration": "900",
                "randomized": true,
                "auid": 21603,
                "questions": [
                    {
                        "qid": "1",
                        "type": "text",
                        "slot": 1,
                        "correct": "2 x 30/20 and 2 x 40/10",
                        "question": "<p>What is the range of the detectors on the tower.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "2",
                        "type": "radio",
                        "slot": 1,
                        "correct": 2,
                        "question": "<p>How long will standard battery’s last on the Tower.</p>",
                        "options": ["1", "2", "3", "4"],
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "3",
                        "type": "text",
                        "slot": 1,
                        "correct": "100.",
                        "question": "<p>Max footfall for the turnstile.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "4",
                        "type": "text",
                        "slot": 1,
                        "correct": "BREEAM (BRE Environmental Assessment Method) is the leading and most widely used environmental assessment method for buildings and communities.",
                        "question": "<p>What dose BREEAM relate to.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "5",
                        "type": "text",
                        "slot": 1,
                        "correct": "480",
                        "question": "<p>How many devices can you add to the fire system.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "6",
                        "type": "text",
                        "slot": 1,
                        "correct": "12m",
                        "question": "<p>Range of the cameras in the POD.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "7",
                        "type": "text",
                        "slot": 1,
                        "correct": "Centre for the Protection of National Infrastructure (CPNI)",
                        "question": "<p>What does CPNI stand for.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "8",
                        "type": "text",
                        "slot": 1,
                        "correct": "Delivery Management System",
                        "question": "<p>What is DMS.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "9",
                        "type": "text",
                        "slot": 1,
                        "correct": "Approx. 30 days depending on quality and amount of statics.",
                        "question": "<p>How long will a DVR record for.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "10",
                        "type": "text",
                        "slot": 1,
                        "correct": "Reboots the router at 6am every day and anytime the sim loses connection to the internet.",
                        "question": "<p>What does the IP Reboot do.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "11",
                        "type": "text",
                        "slot": 1,
                        "correct": "Biometric and Card.",
                        "question": "<p>Name 2 types of readers we use on the turnstile.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "12",
                        "type": "text",
                        "slot": 1,
                        "correct": "The British Standard for detector activated remotely monitored CCTV systems is BS 8418:2015 Installation and remote <br> monitoring of detector-activated CCTV systems – Code of practice",
                        "question": "<p>What is BS8418.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "13",
                        "type": "text",
                        "slot": 1,
                        "correct": "Sounders/Strobes and text message.",
                        "question": "<p>How does the fire panel communicate in the event of a fire.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "14",
                        "type": "text",
                        "slot": 1,
                        "correct": "Depends when the site shut the power down. Will do approx.48 to 56hrs.",
                        "question": "<p>Will double bank batteries last the weekend.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "15",
                        "type": "text",
                        "slot": 1,
                        "correct": "110v or 240v",
                        "question": "<p>What power is required for the Vstile.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "16",
                        "type": "text",
                        "slot": 1,
                        "correct": "72hrs.",
                        "question": "<p>What is our SLA for non-critical failures.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "17",
                        "type": "text",
                        "slot": 1,
                        "correct": "They will put the detectors into tamper and throw off all the pre-sets.",
                        "question": "<p>Why should the site never move the tower by themselves.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "18",
                        "type": "text",
                        "slot": 1,
                        "correct": "If a card reader is fitted and the card is compatible.",
                        "question": "<p>Can you use a CSCS card on the turnstile.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "19",
                        "type": "text",
                        "slot": 1,
                        "correct": "1km in open air.",
                        "question": "<p>What is the range of the Fire equipment.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "20",
                        "type": "text",
                        "slot": 1,
                        "correct": "NONE",
                        "question": "<p>What area of the country do we not deploy to.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "21",
                        "type": "text",
                        "slot": 1,
                        "correct": "5 days",
                        "question": "<p>Standard notice period for a recon.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "22",
                        "type": "text",
                        "slot": 1,
                        "correct": "The detector that covers the 5metres below the tower.",
                        "question": "<p>What is a creep zone.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "23",
                        "type": "text",
                        "slot": 1,
                        "correct": "Cable or 4G.",
                        "question": "<p>How does the laptop connect to the Vstile.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "24",
                        "type": "text",
                        "slot": 1,
                        "correct": "Fire system not connected to the monitoring station.",
                        "question": "<p>What is Bells only.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "25",
                        "type": "text",
                        "slot": 1,
                        "correct": "5 Days",
                        "question": "<p>Standard notice for an install.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "26",
                        "type": "text",
                        "slot": 1,
                        "correct": "Cover the damage to site if an incident is missed.",
                        "question": "<p>What is the insurance charge for.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "27",
                        "type": "text",
                        "slot": 1,
                        "correct": "Min 72hrs.",
                        "question": "<p>How long before an install should your site survey be handed in.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "28",
                        "type": "text",
                        "slot": 1,
                        "correct": "The joint code of practice is for the protection from fire on construction sites and buildings undergoing renovation.",
                        "question": "<p>What is the joint code of practice.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "29",
                        "type": "text",
                        "slot": 1,
                        "correct": "100m",
                        "question": "<p>How far can a PTZ see at night.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "30",
                        "type": "text",
                        "slot": 1,
                        "correct": "UK Timber Frame association",
                        "question": "<p>What does UKTFA stand for.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "31",
                        "type": "text",
                        "slot": 1,
                        "correct": "Vstile, Connect, Tablet",
                        "question": "<p>What Biometric options do we sell.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "32",
                        "type": "text",
                        "slot": 1,
                        "correct": "7",
                        "question": "<p>What is the recommended amount of static cameras you can add to a tower.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "33",
                        "type": "text",
                        "slot": 1,
                        "correct": "3 months",
                        "question": "<p>How frequently do we attend to change the Batteries and SD card in the time Lapse.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "34",
                        "type": "text",
                        "slot": 1,
                        "correct": "Battery backup.",
                        "question": "<p>Main differences between a site view and a tower.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "35",
                        "type": "text",
                        "slot": 1,
                        "correct": "110v or 240v",
                        "question": "<p>What power is required for a tower.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "36",
                        "type": "text",
                        "slot": 1,
                        "correct": "24hrs",
                        "question": "<p>What is our SLA for a critical failure.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "37",
                        "type": "text",
                        "slot": 1,
                        "correct": "Complete tower failure or failure of the PTZ.",
                        "question": "<p>What is a critical failure.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "38",
                        "type": "text",
                        "slot": 1,
                        "correct": "15mins per day.",
                        "question": "<p>How long do we allow the site teams to view their cameras for.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "39",
                        "type": "text",
                        "slot": 1,
                        "correct": "Someone that passes through the turnstile has to exit by the same means or they will not be allowed back in.",
                        "question": "<p>What is anti-pass back.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "40",
                        "type": "text",
                        "slot": 1,
                        "correct": "4G Sim.",
                        "question": "<p>How does the tower transmit video to the monitoring station.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "41",
                        "type": "text",
                        "slot": 1,
                        "correct": "40m in distance and width 10m",
                        "question": "<p>What does 40/10 mean.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "42",
                        "type": "text",
                        "slot": 1,
                        "correct": "30 days.",
                        "question": "<p>Standard notice for a decommission.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "43",
                        "type": "text",
                        "slot": 1,
                        "correct": "Sites.",
                        "question": "<p>Whose responsibility is it to provide power to the CCTV system.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "44",
                        "type": "text",
                        "slot": 1,
                        "correct": "3",
                        "question": "<p>How many health checks are carried out daily on the CCTV systems.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "45",
                        "type": "text",
                        "slot": 1,
                        "correct": "Police unless told otherwise",
                        "question": "<p>In the event of an Intrusion who does the monitoring station call.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "46",
                        "type": "text",
                        "slot": 1,
                        "correct": "A software which collects all the information from the coming and goings of site personal and breaks down into reports.",
                        "question": "<p>What is time and attendance.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "47",
                        "type": "text",
                        "slot": 1,
                        "correct": "250m",
                        "question": "<p>Max distance you can run power.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "48",
                        "type": "text",
                        "slot": 1,
                        "correct": "2.4m-2.6m",
                        "question": "<p>Optimum height of the static and detectors.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "49",
                        "type": "text",
                        "slot": 1,
                        "correct": "35m",
                        "question": "<p>Range of the static cameras.</p>",
                        "images": "",
                        "answers": []
                    },
                    {
                        "qid": "50",
                        "type": "text",
                        "slot": 1,
                        "correct": "300m",
                        "question": "<p>Max distance you can run a static.</p>",
                        "images": "",
                        "answers": []
                    }
                ]
            }
        ];
        return this.myQuiz;
    };
    QuestionBankProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], QuestionBankProvider);
    return QuestionBankProvider;
}());
export { QuestionBankProvider };
//# sourceMappingURL=question-bank.js.map