import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import * as firebase from 'Firebase';
/*
  Generated class for the QuestionBankProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QuestionBankProvider {
  public myQuiz = []; 
  public ref = firebase.database().ref('quizes/');
  constructor() {}

  getData(callback) {
    this.ref.on('value', resp => {
      this.myQuiz = [];
      this.myQuiz = snapshotToArray(resp);
      callback(this.myQuiz);
    });
  }
}

export const snapshotToArray = snapshot => {
    let returnArr = [];

    snapshot.forEach(childSnapshot => {
        let item = childSnapshot.val();
        item.key = childSnapshot.key;
        returnArr.push(item);
    });

    return returnArr;
};


